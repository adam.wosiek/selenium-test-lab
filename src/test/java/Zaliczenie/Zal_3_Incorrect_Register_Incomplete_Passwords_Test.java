package Zaliczenie;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_3_Incorrect_Register_Incomplete_Passwords_Test extends SeleniumBaseTest2 {

    @DataProvider
    public static Object[][] incompletePasswords() {
        return new Object[][]{
                {"LUBIEPLACKI2@", "Passwords must have at least one lowercase ('a'-'z')."},
                {"lubieplacki2@", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"LUBIEplacki@", "Passwords must have at least one digit ('0'-'9')."},
                {"LUBIEplacki2", "Passwords must have at least one non alphanumeric character."},
                {"Lu12@", "The Password must be at least 6 and at max 100 characters long."}
        };
    }

    @Test(dataProvider = "incompletePasswords")

    public void incompletePasswordTest(String password, String error) {
        new LoginPage(driver)
                .openCreateAccountPage()
                .typeEmail("test@test.com")
                .typePassword(password)
                .typeConfirmPassword(password)
                .submitRegister()
                .assertPasswordError(error);
    }
}
