package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_4_Correct_Login_Test extends SeleniumBaseTest2 {

    @Test
    public void correctLoginTest() {
        new LoginPage(driver)

                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertHomePageIsShown();
    }
}