package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;
import java.util.UUID;

public class Zal_1_Correct_Register_Test extends SeleniumBaseTest2 {

    @Test
    public void correctRegisterTest() {
        new LoginPage(driver)
                .openCreateAccountPage()
                .typeEmail(UUID.randomUUID().toString() + "@test.com")
                .typePassword("Haslo2@")
                .typeConfirmPassword("Haslo2@")
                .submitRegister()
                .assertHomePageIsShown();
    }
}