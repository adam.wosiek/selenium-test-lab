package Zaliczenie;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import config.Config;

import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest2 {

    protected WebDriver driver;
    protected Config config;


    @BeforeMethod
    public void baseBeforeMethod () {
        config = new Config();
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        // System.setProperty("webdriver.chrome.driver", "c:/tools/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");
        //driver.get("http://statisticalprocesscontrol.azurewebsites.net");
    }

    @AfterMethod
        public void baseAfterMethod () {
        driver.quit();
    }
}
