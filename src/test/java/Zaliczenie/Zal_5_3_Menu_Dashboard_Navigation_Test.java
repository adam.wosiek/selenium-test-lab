package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_5_3_Menu_Dashboard_Navigation_Test extends SeleniumBaseTest2 {

    @Test
    public void MenuDashboardTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToDaschboard()
                .assertDashboardUrl("http://localhost:4444/")
                .assertHomePageIsShown();
    }
}