package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_5_1_Menu_Processes_Navigation_Test extends SeleniumBaseTest2 {

    @Test
    public void MenuProcessesTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .assertProcessesUrl("http://localhost:4444/Projects");
    }
}