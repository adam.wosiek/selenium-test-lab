package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;

public class Zal_5_2_Menu_Characteristics_Navigation_Test extends SeleniumBaseTest2 {

    @Test
    public void MenuCharacteristicsTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToCharacteristics()
                .assertCharacteristicsUrl("http://localhost:4444/Characteristics");
    }
}