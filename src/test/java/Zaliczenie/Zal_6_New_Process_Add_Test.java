package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;


public class Zal_6_New_Process_Add_Test extends SeleniumBaseTest2 {


    @Test
    public void addNewProcess() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeProcessName()
                .typeProcessDescription()
                .typeProcessNotes()
                .submitCreateProcess()
                .assertProcess("Nazwa projektu","Opis projektu","Notatki do projektu" );
    }
}