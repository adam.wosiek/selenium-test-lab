package Zaliczenie;

import org.testng.annotations.Test;
import pages.LoginPage;


public class Zal_2_Incorrect_Register_Incompatible_Passwords_Test extends SeleniumBaseTest2 {

    @Test
    public void IncorrectRegisterTest() {
        new LoginPage(driver)
                .openCreateAccountPage()
                .typeEmail("test@test.com")
                .typePassword("Haslo2@")
                .typeConfirmPassword("Haslo")
                .submitRegister()
                .assertIncompatiblePasswordError();
    }
}

