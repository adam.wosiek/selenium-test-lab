import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import java.util.concurrent.TimeUnit;

public class Lab_8_Test_Poprawnego_Logowania_POP_Test extends SeleniumBaseTest {

   // private WebDriver driver;

    @Test
    public void correctLoginTest () {
  // System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
 //  WebDriver driver = new ChromeDriver();
  // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  // driver.manage().window().maximize();
  // driver.get("http://localhost:4444/");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("test@test.com");
        loginPage.typePassword("Test1!");
        HomePage homePage = loginPage.submitLogin();
        Assert.assertTrue(homePage.welcomeElm.getText().contains("Welcome"));
        driver.quit();
    }
}
