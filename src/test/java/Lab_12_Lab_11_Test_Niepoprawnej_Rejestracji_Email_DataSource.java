import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;

import java.util.concurrent.TimeUnit;

public class Lab_12_Lab_11_Test_Niepoprawnej_Rejestracji_Email_DataSource extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object [][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest(String wrongEmail) {
        CreateAccountPage  CreateAccountPage = new CreateAccountPage(driver);
        CreateAccountPage.typeEmail(wrongEmail);
//      Dlaczego ponisza linijka musi byc zdublowana?
        CreateAccountPage.CreateAccountWithFailure();
        CreateAccountPage.CreateAccountWithFailure();
        Assert.assertTrue( CreateAccountPage.loginErrors.get(0).getText().contains("The Email field is not a valid e-mail address."));
    }
}
