import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

public class Lab_12_Lab_8_Test_Poprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void correctLoginTest () {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("test@test.com");
        loginPage.typePassword("Test1!");
        HomePage homePage = loginPage.submitLogin();
        Assert.assertTrue(homePage.welcomeElm.getText().contains("Welcome"));
    }
}
