import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_5_Test_Niepoprawnego_Logowania_Email_Test extends SeleniumBaseTest {
    @Test
    public void incorrectLoginTestWrongEmail() {
      //  System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
      //  System.setProperty("webdriver.chrome.driver", "c:/tools/drivers/chromedriver.exe");
       // WebDriver driver = new ChromeDriver();
      //  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
     //  driver.manage().window().maximize();
       // driver.get("http://localhost:4444/");
        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys("test");
        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("Test1!");
        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();
        WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");
        //old school walidacja czy zadanny blad istnieje na liscie  bledow
//        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>li"));
//
//        boolean doesErrorExists = false;
//
//        for (int i=0; i<validationErrors.size(); i++) {
//        if (validationErrors.get(1).getText().equals("The Email field is not a valid e-mail address.")){
//            doesErrorExists = true;
//            break;
//        }
//        }

        // New school (JAVA 8 I WYZEJ) walidacja czy zadany blad istneije na liscie bledow.
        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors li"));
        boolean doesErrorExists = validationErrors
                .stream()
                .map(validationError -> validationError.getText())
                .anyMatch( error -> error.equals("The Email field is not a valid e-mail address."));
        Assert.assertTrue(doesErrorExists);
    }
}
