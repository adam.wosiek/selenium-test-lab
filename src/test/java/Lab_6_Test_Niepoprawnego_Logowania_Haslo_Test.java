import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_6_Test_Niepoprawnego_Logowania_Haslo_Test extends SeleniumBaseTest {
    @Test
    public void incorrectLoginTestWrongEmail() {
     //   System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
    //    WebDriver driver = new ChromeDriver();
     //   driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
     //   driver.manage().window().maximize();
     //   driver.get("http://localhost:4444/");
        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys("test@test.com");
        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("Tes");
        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();
//        WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
//        Assert.assertEquals(emailError.getText(), "Invalid login attempt.");

        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors li"));
        boolean doesErrorExists = validationErrors
                .stream()
                .map(validationError -> validationError.getText())
                .anyMatch( error -> error.equals("Invalid login attempt."));
        Assert.assertTrue(doesErrorExists);
    }
}
