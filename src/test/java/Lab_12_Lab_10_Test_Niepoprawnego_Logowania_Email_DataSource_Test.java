import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_12_Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource_Test extends SeleniumBaseTest {


    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object [][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test (dataProvider = "getWrongEmails")
    public void incorrectEmailTest(String wrongEmail) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail(wrongEmail);
//        Dlaczego ponisza linijka musi byc zdublowana?
        loginPage.submitLoginWithFailure();
        loginPage.submitLoginWithFailure();
        Assert.assertTrue(loginPage.loginErrors.get(0).getText().contains("The Email field is not a valid e-mail address."));
    }

}