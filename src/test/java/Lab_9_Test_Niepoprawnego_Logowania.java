import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_9_Test_Niepoprawnego_Logowania
        //extends SeleniumBaseTest
{

        private WebDriver driver;

        @Test
        public void incorrectLoginTest () {
          System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
           WebDriver driver = new ChromeDriver();
           driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
          driver.manage().window().maximize();
          driver.get("http://localhost:4444/");
            LoginPage loginPage = new LoginPage(driver);
            loginPage.typeEmail("test2@test.com");
            loginPage.typePassword("Test1!");
            loginPage.submitLoginWithFailure();
            Assert.assertTrue(loginPage.loginErrors.get(0).getText().contains("Invalid login attempt."));
            driver.quit();
        }
    }
