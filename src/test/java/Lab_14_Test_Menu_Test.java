import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Test_Menu_Test extends SeleniumBaseTest {
    @Test
    public void  menuTest(){
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
             .submitLogin()
            .goToProcesses()
                .assertProcessesTitle()
                    .assertProcessesUrl("http://localhost:4444/Projects")
               .goToCharacteristics()
                   .assertCharacteristicsUrl("http://localhost:4444/Characteristics")
                   .assertCharacteristicsHeader().goToDaschboard()
                   .assertDashboardUrl("http://localhost:4444/")
                   .assertDemoProjectIsShown();


    }
}
