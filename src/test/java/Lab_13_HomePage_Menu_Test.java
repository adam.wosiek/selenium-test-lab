import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.ProcessesPage;

public class Lab_13_HomePage_Menu_Test extends SeleniumBaseTest {

@Test
    public void  goToProcessesTest () {
    new LoginPage(driver)
            .typeEmail("test@test.com")
            .typePassword("Test1!")
            .submitLogin()
            .goToProcesses();
    }
}

