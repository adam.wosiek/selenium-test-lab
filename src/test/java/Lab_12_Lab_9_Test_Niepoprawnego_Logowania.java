import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Lab_12_Lab_9_Test_Niepoprawnego_Logowania extends SeleniumBaseTest {

        @Test
        public void incorrectLoginTest () {
            LoginPage loginPage = new LoginPage(driver);
            loginPage.typeEmail("test2@test.com");
            loginPage.typePassword("Test1!");
            loginPage.submitLoginWithFailure();
            Assert.assertTrue(loginPage.loginErrors.get(0).getText().contains("Invalid login attempt."));
        }
    }
