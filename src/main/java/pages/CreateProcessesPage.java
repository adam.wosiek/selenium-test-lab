package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.UUID;

public class CreateProcessesPage extends ProcessesPage {


    public CreateProcessesPage(WebDriver driver) { super(driver); }

    @FindBy(id = "Name")
    private WebElement processName;

    @FindBy(id = "Description")
    private WebElement projectDescription;

    @FindBy(id = "Notes")
    private WebElement projectNotes;

    @FindBy(css ="input[type=submit]")
    private WebElement createBtn;



    public CreateProcessesPage typeProcessName() {
        processName.sendKeys("Nazwa projektu");
        return new CreateProcessesPage (driver);
    }

    public CreateProcessesPage typeProcessDescription() {
        projectDescription.sendKeys("Opis projektu");
        return new CreateProcessesPage (driver);
    }

    public CreateProcessesPage typeProcessNotes() {
        projectNotes.sendKeys("Notatki do projektu");
        return new CreateProcessesPage (driver);
    }

    public CreateProcessesPage submitCreateProcess() {
        createBtn.click();
        return new CreateProcessesPage (driver);
    }

    public CreateProcessesPage submitCreateWithFailure() {
        createBtn.click();

        return this;
    }


}
