package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class HomePage {
    protected WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
        @FindBy(css=".profile_info>h2")
        public WebElement welcomeElm;
//        @FindBy(css=".validation-summary-errors>ul>li")
//        public List<WebElement>loginErrors;

    @FindBy(css=".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css="a[href$=Projects]")
    private WebElement processesMenu;

    @FindBy(css="a[href$=Characteristics]")
    private WebElement characteristicsMenu;

    @FindBy(css=".menu-home")
    private WebElement homeNav;

    @FindBy(css="a[href$='/']")
    private WebElement dashboardMenu;

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeMessage;



    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));
        return parent.getAttribute("class").contains("active");
    }

    public ProcessesPage goToProcesses (){
        if(!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));

        processesMenu.click();

        return new ProcessesPage(driver);
    }


    public DashboardPage goToDaschboard (){
        if(!isParentExpanded(homeNav))
            homeNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
       wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));

        dashboardMenu.click();

        return new DashboardPage(driver);
    }
    public CharacteristicsPage goToCharacteristics () {
        if (!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        //   wait.until(ExpectedConditions.elementToBeClickable(characteristicsMenu));

        characteristicsMenu.click();

        return new CharacteristicsPage(driver);
    }
        public HomePage assertHomePageIsShown(){
            Assert.assertTrue(driver.getCurrentUrl().equals("http://localhost:4444/"));
            return this;
        }


    }

