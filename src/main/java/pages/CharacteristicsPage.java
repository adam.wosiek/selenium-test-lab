package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {
    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(css = ".page-title h3")
    private WebElement CharacteristicsHeader;


    public CharacteristicsPage assertCharacteristicsHeader() {
        Assert.assertEquals(CharacteristicsHeader.getText(), "Characteristics");
        return this;
    }

    public CharacteristicsPage assertCharacteristicsUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;


    }
}
