package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage {
    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    @FindBy(className = "submit")
    private WebElement registerButton;

    @FindBy(id = "ConfirmPassword-error")
    private WebElement incompatiblePasswordError;

    @FindBy(css = ".validation-summary-errors>ul>li")
    private WebElement passwordError;




    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage typeConfirmPassword(String confirmPassword) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(confirmPassword);
        return this;
    }

    public CreateAccountPage CreateAccountWithFailure() {
        loginBtn.click();
        return this;
    }

    public CreateAccountPage submitRegister() {
        registerButton.click();
        return this;
    }

    public CreateAccountPage assertIncompatiblePasswordError (){
        Assert.assertTrue(incompatiblePasswordError.getText().contains("The password and confirmation password do not match."));
        return this;
    }

    public HomePage assertHomePageIsShown(){
        Assert.assertTrue(driver.getCurrentUrl().equals("http://localhost:4444/"));
        return new HomePage(driver);
    }

    public CreateAccountPage assertPasswordError(String error) {
        Assert.assertEquals(passwordError.getText(), error);
        return this;
    }

}

