package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {
    public DashboardPage(WebDriver driver) { super(driver); }

    @FindBy(css = ".x_title h2")
    private WebElement DashboardHeader;



    public DashboardPage assertDemoProjectIsShown() {
        Assert.assertEquals(DashboardHeader.getText(), "DEMO PROJECT");
        return this;
    }
    public DashboardPage assertDashboardUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);

        return this;
    }



}
