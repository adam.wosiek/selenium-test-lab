package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
// nie wymyslaj selektorow np. nie ma selektora loginbtn, trzeba sprawdzic jaki jest naprawde
    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;
// do tego ponizej podmienic dane - pkt 10. lab 7

    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPasswordTxt;


    @FindBy(css = ".change_link>a")
    private WebElement registerBtn;



    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }


    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public CreateAccountPage openCreateAccountPage() {
        registerBtn.click();
        return new CreateAccountPage(driver);
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }
    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }



}

