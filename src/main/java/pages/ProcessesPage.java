package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProcessesPage extends HomePage {

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".page-title h3")
    private WebElement processesTitle;

    @FindBy(css = "a{href*=create]")
    private WebElement processName;

    @FindBy(css = "a[href$=Characteristics]")
    private WebElement demoProjectIsShown;

   @FindBy(linkText = "Add new process")
    private WebElement addProcess;

    public ProcessesPage assertProcessesUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }

    public ProcessesPage assertProcessesTitle() {
        Assert.assertEquals(processesTitle.getText(), "Processes");
        return this;
    }


    public ProcessesPage assertDemoProjectIsShown() {
        Assert.assertEquals(demoProjectIsShown.getText(), "Characteristics");
        return this;
    }

    public CreateProcessesPage clickAddProcess() {
        addProcess.click();
        return new CreateProcessesPage(driver);
    }

    public ProcessesPage assertProcess (String expName, String expDescription, String expNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);

        WebElement processRow = driver.findElement(By.xpath(processXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);

        return this;
    }


}

